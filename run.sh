#!/bin/bash

PID=$(ps aux | grep CyberScoreClient | grep -v grep | awk '{print $2}')

if [ ! -z $PID ]; then
	kill $PID
fi

if [[ $1 == "-a" ]]; then
	go-assets-builder \
		templates/ \
		assets/css/ \
		assets/js/min/ \
		assets/js/ \
		assets/fonts/ \
		config/local.json \
		-o vfs/assets.go \
		-p vfs

else
	go install
fi

	CyberScoreClient &> gin.log &
