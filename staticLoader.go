package main

import (
	"CyberScoreClient/vfs"
	"github.com/gin-gonic/gin"
	"strings"
	"log"
)

type StaticPath struct {
	aType string	// Asset type (CSS/JS/IMG)
	dir string		// Asset directory (bootstrap, fontawesome, fonts, etc.)
	file string		// Asset file name
}

// Loads static files and returns []byte data to the router
func staticLoader(c *gin.Context, pathStruct StaticPath) (status int, contentType string, data []byte) {

	// Build file path
	var pathStr string
	if pathStruct.dir == "" {
		pathStr = "/assets/" + pathStruct.aType + "/" + pathStruct.file
	} else {
		pathStr = "/assets/" + pathStruct.aType + "/" + pathStruct.dir + pathStruct.file
	}

	fileArr := strings.Split(pathStruct.file, ".")
	fileType := fileArr[len(fileArr) - 1] // Get last element of array (file suffix)

	// Assigns correct MIME type based on file type
	switch fileType {
	case "css":
		contentType = "text/css"
	case "js":
		contentType = "application/javascript"
	case "json":
		contentType = "application/json"
	case "map":
		contentType = "application/json"
	case "png":
		contentType = "image/png"
	case "gif":
		contentType = "image/gif"
	case "jpg":
		contentType = "image/jpeg"
	case "jpeg":
		contentType = "image/jpeg"
	case "webp":
		contentType = "image/webp"
	case "svg":
		contentType = "image/svg+xml"
	case "otf":
		contentType = "application/x-font-otf"
	case "ttf":
		contentType = "application/x-font-ttf"
	case "woff":
		contentType = "application/x-font-woff"
	default:
		contentType = "application/octet-stream"
	}

	data, err := vfs.Search(pathStr) // Get byte data of file


	// Return 404 if error exists
	if err != nil {
		log.Printf("Could get retrieve file data: %v", err)
		return 404, contentType, nil
	} else {
		return 200, contentType, data
	}
}
