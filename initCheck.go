package main

import (
	"os"
	"io/ioutil"
	"encoding/json"
	"log"
	"github.com/gin-gonic/gin"
)

// Basic checks triggered by browsing to the webroot
func initCheck(c *gin.Context) {
	configFile, err := os.Open("config/local.json")
	if err != nil {
		log.Printf("Could not open config file: %v", err)
	}

	var configJson map[string]interface{}
	byteValue, err := ioutil.ReadAll(configFile)
	if err != nil {
		log.Printf("Could not read file contents: %v", err)
	}

	configFile.Close()

	json.Unmarshal([]byte(byteValue), &configJson)

	if configJson["newProject"].(bool) {
		c.Redirect(303, "/start")
	} else {
		c.Redirect(303, "/dashboard")
	}
}
