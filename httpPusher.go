package main

import (
	"github.com/gin-gonic/gin"
	"errors"
	"log"
)

// Accepts gin.Context and multiple assets and pushes each asset to the client
// Returns err on failure and nil on success
func httpPush(c *gin.Context, assets []string) error {
	pusher := c.Writer.Pusher() // Create http2 pusher

	// Check if pusher is supported
	if pusher == nil {
		return errors.New("HTTP/2 Pusher not supported.")
	}

	var err error
	for _, asset := range assets {
		err := pusher.Push(asset, nil) // Push target to the client
		if err != nil {
			log.Printf("Could not push %s: %v", asset, err)
		}
	}

	if err != nil {
		return err
	}
	return nil
}
