package vfs

import (
	"html/template"
	"io/ioutil"
	"errors"
	"strings"
	"fmt"
)

// Loads HTML templates from go-assets virtual FS
func LoadTemplate() (*template.Template, error) {
	t := template.New("")
	for name, file := range Assets.Files {
		if file.IsDir() {
			continue
		}

		fileArr := strings.Split(name, ".")
		fileType := fileArr[len(fileArr) - 1] // Get last element of array (file suffix)

		// Checks if this is an HTML file
		if fileType != "html" {
			continue
		}

		data, err := ioutil.ReadAll(file)
		if err != nil {
			return nil, err
		}
		t, err = t.New(name).Parse(string(data))
		if err != nil {
			return nil, err
		}
	}
	return t, nil
}

// Read for files and returns byte data if found, nil if not found.
func Search(path string) ([]byte, error) {

	// Strips "/" from end of path if found
	if path[len(path) - 1:] == "/" {
		path = string(path[len(path) - 1])
	}

	// Prepends "/" to path if it does not exist
	if !strings.HasPrefix(path, "/") {
		path = fmt.Sprintf("/%s", path)
	}

	// Iterates over every file in the vFS
	for name, file := range Assets.Files {

		// Continue if the current file doesn't match path
		if name != path {
			continue
		} else {
			if file.IsDir() {
				return nil, errors.New(path + " is a directory.")
			}

			return file.Data, nil
		}
	}

	// Shouldn't get here unless file doesn't exist
	return nil, errors.New(path + " not found.")
}
