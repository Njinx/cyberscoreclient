Sentry.init({
	dsn: 'https://56f2595bffbf436593d4d3824925483c@sentry.io/1431640',
	release: '1.0'
});

sel = function(selector) {
	return document.querySelectorAll(selector);
}

function msToISO(ms) {
	return new Date(ms).toISOString().slice(11, -5) + ' (HH:MM:SS)';
}

var projectState;

// Check if a competition is already running
cash(window).ready(function() {
	var request = {};
	request = JSON.stringify(request);
	APIRequest('project/getCompData', request, function(response) {
		if (response.error != 0) {
			console.log(response.error);
			Sentry.captureException(response.error);
		} else {
			projectState = response.body;

			// If a competition is running
			if (projectState.state == 1) {
				loadCompData();
				createTimer();
			}
		}
	});
});

cash(sel('#start-project')).on('click', function() {
	if (this.dataset.confirm == 0) {
		this.dataset.confirm = 1;
		this.innerHTML = '<h1>Start!</h1>';
	} else if (this.dataset.confirm == 1) {
		var request = {
			"body": sel('#projectID')[0].dataset.id,
		};
		request = JSON.stringify(request);

		APIRequest('project/start', request, function(response) {
			if (response.error != 0) {
				console.log(response.error);
				Sentry.captureException(response.error);
			} else {
				response.body = JSON.parse(response.body);

				projectState = response.body;

				if (projectState.state) {
					loadCompData();
					createTimer();
				}
			}
		});
	}
});

// Loads in the current projectState into the HTML fields
function loadCompData() {

	for (var key in projectState.scoreboard) {
		if (sel(`[data-id=${key}]`).length > 0) {

			// If the JSON key is a time value, convert to seconds
			if (key == 'timeRemaining') {
				sel(`[data-id=${key}]`)[0].innerHTML = msToISO(projectState.scoreboard[key]);
			} else {
				sel(`[data-id=${key}]`)[0].innerHTML = projectState.scoreboard[key];
			}
		}
	}
}

function createTimer() {
	var compClock = window.setInterval(function() {

		var request = {};
		request = JSON.stringify(request);
		APIRequest('project/getCompData', request, function(response) {
			if (response.error != 0) {
				console.log(response.error);
				Sentry.captureException(response.error);
			} else {
				projectState = response.body;
				console.log(projectState);
				updateVulnList(projectState.scoreboard.vulnsFixedArray);
			}
		});

		// Update the score, vulnerabiliies fixed, and remaining time
		sel('[data-id=pointsCurrent]')[0].innerHTML = projectState.scoreboard.pointsCurrent;
		sel('[data-id=timeRemaining]')[0].innerHTML = msToISO(projectState.scoreboard.timeRemaining);
		sel('[data-id=vulnsFixed]')[0].innerHTML = projectState.scoreboard.vulnsFixed;

		// If time has run out
		if (Math.round((new Date()).getTime() / 1000) >= projectState.scoreboard.finishTime) {
			window.clearInterval(compClock);
			alert("Times up!");
		}
	}, projectState.scoreboard.refreshInt);
}

// Updates the Vulnerability list
function updateVulnList(vulns) {

	// Clears the vulnList to prevent duplicates
	cash(sel('#vulnList')).empty();

	for (var key in vulns) {
		var vuln = vulns[key];

		// If the vulnerability was fixed
		if (vuln.pointChange != 0) {

			// Text color green on point gain; Red on loss
			var color = (vuln.pointChange > 0) ? '#28B463' : '#C0392B';

			var entry = `<p id="${vuln.id}" style="color:${color}">${vuln.pointChange}: ${vuln.title}</p>`;
			cash(sel('#vulnList')).append(entry);
		}
	}
}
