var newProjectBtn = document.getElementById('new-project');
var importConfigBtn = document.getElementById('import-config');

newProjectBtn.onclick = function() {
	request = '{"body":"newProject"}';

	APIRequest('config/startHTML', request, function(response) {
		//var responseBody = xhr.responseText;
		if (response.body.includes('location: ')) {
			// Gets the location to redirect to
			var redirectLocation = response.body.slice(9);
			window.location = redirectLocation;
		} else {
			console.log('Invalid response: \"' + response.body + '\"');
		}
	});
};
