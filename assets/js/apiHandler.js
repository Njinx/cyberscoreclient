// Endpoint doesn't include full URL
// localhost:7676/v1/generic/simpleResponse becomes generic/simpleResponse
function APIRequest(endpoint, request, callback = null) {
	var url = "http://127.0.0.1:7676/v1/" + endpoint;

	// Uses older library for IE 6 and older
	xhr = window.XMLHttpRequest
		? new XMLHttpRequest()
		: new ActiveXObject("Microsoft.XMLHTTP");

	try {
		xhr.open('POST', url, true);
		xhr.send(request);
	} catch(err) {
		Sentry.captureException(err);
	}

	xhr.onreadystatechange = function() {

		// xhr.readyState states
		let UNSENT				= 0;
		let OPENED				= 1;
		let HEADERS_RECIEVED	= 2;
		let LOADING				= 3;
		let DONE				= 4;

		let OK = 200;

		if ((xhr.status == OK) && (xhr.readyState == DONE)) {
			if (callback != null) {

				// Parse JSON response into object
				var response = JSON.parse(xhr.responseText);

				try {
					response.body = atob(response.body);			// Decodes the response from Base64 to ASCII
				} finally {
					callback(response);
				}
			}
		}
	}
}

// FilePath	: Path of the file to be loaded
// element	: Element to replace with
function AJAXLoad(filePath, elementID) {
	var elementObj = document.getElementById(elementID);

	var request = {
		"body": filePath
	};
	request = JSON.stringify(request);

	APIRequest("file/loadFile", request, function(response) {
		if (response.error != 0) {
			console.log("Error loading HTML: " + response.error);
		} else if (response.body == null) {
			console.log("No response");
		} else {
			// Decodes the response from Base64 to ASCII
			var decodedResp = atob(response.body);

			// If the page data couldn't be loaded, display an error
			if (decodedResp == null) {
				elementObj.innerHTML = "<h1>Could not load form. Try refreshing.</h1>";
			} else {
				elementObj.innerHTML = decodedResp;
			}
		}
	});
}
