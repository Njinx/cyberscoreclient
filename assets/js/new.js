Sentry.init({
	dsn: 'https://56f2595bffbf436593d4d3824925483c@sentry.io/1431640',
	release: '1.0'
});

sel = function(selector) {
	return document.querySelectorAll(selector);
}

let CMP_HASH = 0;
let CHECK_CMD = 1;

let FORM_DETAILS = 1;
let FORM_VULNS = 2;

let PAGE_MAX = FORM_VULNS;
let PAGE_MIN = FORM_DETAILS;

var currentPage = 1;

// On DOM load
cash(document).ready(function() {
	cash(sel('#form-2')).hide();
	displayBtn();

	sel('input[data-maxlength]').forEach(function(element) {
		cash(element).css('width', element.dataset.maxlength + 'em');
	});
});

// inc = true to increment currentPage, false to decrement
function loadPage(inc) {
	if (inc) {
		if (currentPage != PAGE_MAX) {
			currentPage++;
		}
	} else {
		if (currentPage != PAGE_MIN) {
			currentPage--;
		}
	}

	for (var i = 1; i <= PAGE_MAX; i++) {
		var vulnForm = document.querySelectorAll(`#form-${i}`);

		if (i == currentPage) {
			cash(vulnForm).show();
		} else {
			cash(vulnForm).hide();
		}
	}

	displayBtn();
}

function saveForm() {

	// Object containing the entire form
	// basicDetails = Object that contains the project details
	// 		projectName = The name of the project/competition/challenge
	// 		maxTime = The maximum time allowed during before the timer is stopped (milliseconds)
	// 		refreshInt = The interval in which the system will check the vulnerability criterias (milliseconds)
	// vulnCriterias = Array containing the vulnerability critera objects
	var formObj = {
		'basicDetails'	: {
			'projectName'	: '',
			'maxTime'		: 0,
			'refreshInt'	: 0,
		},
		'vulnCriterias'		: []
	};

	// Serialize the Basic Details form
	(function() {
		var formFields = cash(sel('#form-details [data-form=true]'));

		// Grabs the values from the HTML and sets formObj.basicDetails equal to them
		try {
			for (var i = 0; i < formFields.length; i++) {

				// Converts time from HH:MM/MM:SS to milliseconds
				if (formFields[i].dataset.time) {
					var timeRaw = formFields[i].value.split(':')

					switch(formFields[i].dataset.time) {
						case 'hhmm':
							var milliseconds = (timeRaw[0] * 3600000) + (timeRaw[1] * 60000);
							break;
						case 'mmss':
							var milliseconds = (timeRaw[0] * 60000) + (timeRaw[1] * 1000);
							break;
					}
					 formObj['basicDetails'][formFields[i].name] = milliseconds;
				} else {
					formObj['basicDetails'][formFields[i].name] = formFields[i].value;
				}
			}
		} catch(err) {
			Sentry.captureException(err);
		}
	})();

	// Serialize the Vulnerabilities form
	(function() {
		var cmpHashForms = cash(sel('#form-vulns [data-id][data-type=cmpHash]'));
		var checkCmdForms = cash(sel('#form-vulns [data-id][data-type=checkCmd]'));

		try {

			// For each vulnerability criteria section (Compare hashes, Check command output, etc.)
			for (var i = 0; i < cmpHashForms.length; i++) {

				// Object appended to vulnObj.cmpHash
				// Contains the cmpHash vulnerability checking criteria
				// title = The title of the criteria checked
				// id = Base64 id of the vulnerability criteria
				// filePath = The path of the file that's being compared
				// fileHash = The hash being checked against filePath
				// pointChange = Points gained/lost if filePath equals fileHash
				var cmpHashObj = {
					'type'			: 'CMP_HASH',
					'id'			: '',
					'title'			: '',
					'filePath'		: '',
					'fileHash'		: '',
					'pointChange'	: 0
				};
				var checkCmdObj = {};

				var vulnForm = cmpHashForms[i];
				var vulnFormItems = vulnForm.children[2].children;
				cmpHashObj.id = vulnForm.getAttribute('data-id');

				// For each field in the section
				for (var j = 0; j < vulnFormItems.length; j++) {
					var vulnFormField = vulnFormItems[j];

					// Process surface data forms (not radio buttons/checkboxes)
					if (vulnFormField.getAttribute('data-form')) {
						cmpHashObj[vulnFormField.getAttribute('name')] = vulnFormField.innerHTML;
					}

					// Process nested data forms (radio buttons/checkboxes)
					if (
						(vulnFormField.classList.contains('points-select')) &&
						(vulnFormField.getAttribute('checked') == 'true')
					) {
						var pointChangeForm = vulnFormField.children[0];

						if (pointChangeForm.getAttribute('name') == 'pointsGain') {
							cmpHashObj['pointChange'] = Math.abs(pointChangeForm.innerHTML);
						} else {
							cmpHashObj['pointChange'] = Math.abs(pointChangeForm.innerHTML) * -1; // Negate to represent point loss
						}
					}
				}

				// Append the criteria object to vulnObjs
				formObj.vulnCriterias.push(cmpHashObj);
			}
		} catch(err) {
			Sentry.captureException(err);
		}
	})();

	// Submit the form to the backend
	var request = JSON.stringify(formObj);
	APIRequest('project/submitProjectForm', request, function(response) {
		if (response.error != 0) {
			console.log(response.error);
			Sentry.captureException(response.error);
		} else {
			window.location = '/dashboard';
		}
	});
}

function displayBtn() {
	if ((currentPage >= PAGE_MIN) && (currentPage < PAGE_MAX)) {
		cash(sel('#btn-next')).show();
		cash(sel('#btn-prev')).hide();
		cash(sel('#btn-fin')).hide();
	} else if (currentPage == PAGE_MAX) {
		cash(sel('#btn-next')).hide();
		cash(sel('#btn-prev')).show();
		cash(sel('#btn-fin')).show();
	} else {
		cash(sel('#btn-next')).hide();
		cash(sel('#btn-prev')).hide();
		cash(sel('#btn-fin')).hide();
	}
}

// Toggle Vulnerabilities menu
var menuToggle = 0;
function toggleVulnMenu() {

	// If the modal is opened
	if (menuToggle) {
		var translateVal = {
			fab : '+=14vw',
			menu: '+=24vw'
		};
		var bgVal = '#333333';
		var colorVal = '#EFEFEF';
		var opacityVal = 1;
		var durationVal = 100;
		var easeVal = 'linear';
		menuToggle = 0;

	} else {
		var translateVal = {
			fab : '-=14vw',
			menu: '-=24vw'
		};
		var bgVal = '#EFEFEF';
		var colorVal = '#333333';
		var opacityVal = 0;
		var durationVal = 400;
		var easeVal = 'cubicBezier(1,.05,.82,.09)';
		menuToggle = 1;
	}

	anime({
		targets		: '.fab-container',
		translateX	: translateVal.fab,
		easing		: 'easeOutQuad',
		duration	: 400
	});
	anime({
		targets		: '#vuln-menu',
		translateX	: translateVal.menu,									// I'm going insane
		easing		: 'easeOutQuad',
		duration	: 400
	});
	anime({
		targets	: '.fab-text',
		rotate	: '+=45',
		easing	: 'easeOutQuad',
		duration: 400
	});
	anime({
		targets			: '.fab-text',
		color			: colorVal,
		easing			: easeVal,
		duration		: durationVal
	});
	anime({
		targets			: '.fab-border',
		backgroundColor	: bgVal,
		opacity			: opacityVal,
		easing			: easeVal,
		duration		: durationVal
	});
}

// Inserts the vulnerability HTML into the DOM
function htmlVuln(vulnHtml) {
	var request = {
		"body": vulnHtml
	};
	request = JSON.stringify(request);

	APIRequest('file/loadFile', request, function(response) {
		if (response.error != 0) {
			console.log('Error loading HTML: ' + response.error);
		} else if (response.body == null) {
			console.log('No response');
		} else {

			// {[?data-id>]} is a substitute for the vulnerability id
			var randId = btoa(Math.random() * 100);
			randId = randId.slice(0, randId.length - 2);
			response.body = response.body.replace('{[?data-id>]}', randId);

			var nodeList = document.createRange().createContextualFragment(response.body);
			document.getElementById('form-vulns').appendChild(nodeList);
		}
	});
}

function pointsSelector(ev, el) {
	if (ev.target.getAttribute('data-placeholder') == '#') {
		return;
	}

	var isChecked = el.getAttribute('checked');
	var siblings = cash(el).siblings('span.points-select');

	// Toggle the checked state of the element and child
	// Toggle the text-pulse class
	switch(isChecked) {
		case 'true':
			el.setAttribute('checked', false);
			cash(el).children('span').attr('checked', false);
			cash(el).children('span').removeClass('text-pulse');
			break;
		case 'false':
			el.setAttribute('checked', true);
			cash(el).children('span').attr('checked', false);
			cash(el).children('span').addClass('text-pulse');
			break;
	}

	for (var i = 0; i < siblings.length; i++) {
		siblings[i].setAttribute('checked', false);
		cash(siblings[i]).children('span').attr('checked', false);
		cash(siblings[i]).children('span').removeClass('text-pulse');
	}
}

// Add Vulnerability button listener
cash(sel('#btn-vuln')).on('click', function() {
	var vulnId = this.dataset.vuln;

	switch(vulnId) {
		case "CMP_HASH":
			htmlVuln('/templates/new-cmpHash.html');
			break;
		case "CHECK_CMD":
			htmlVuln('/templates/new-checkCmd.html');
			break;
		default:
			console.log("Vulnerability " + vulnId + " not defined.");
			break;
	}
})

// Navigation button listeners
cash(sel('#btn-next')).on('click', function() {
	loadPage(true);
});
cash(sel('#btn-prev')).on('click', function() {
	loadPage(false);
});
cash(sel('#btn-fin')).on('click', function() {
	saveForm();
});

// Enforces field length in input tags
cash(sel('input[data-maxlength]')).on('keyup', function() {
	var limit = this.dataset.maxlength;

	if (this.value.length > limit) {
		this.value = this.value.slice(0, limit);
	}
});
