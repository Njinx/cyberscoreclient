Sentry.init({
	dsn: 'https://56f2595bffbf436593d4d3824925483c@sentry.io/1431640',
	release: '1.0'
});

sel = function(selector) {
	return document.querySelectorAll(selector);
}

cash(document).ready(function() {
	loadProjects();
});

// Loads the projects into #projects-root
function loadProjects() {
	var request = "{}";
	APIRequest('project/getProjectsList', request, function(response) {
		if (response.error != 0) {
			console.log(response.error);
			Sentry.captureException(response.error);
		} else {
			response.body = JSON.parse(response.body);

			for (var i = 0; i < response.body.length; i++) {
				var nameHex = response.body[i].hex;
				var nameASCII = response.body[i].ascii;
				var entry = `<a href="project/${nameHex}">${nameASCII}</a>`

				cash(sel('#projects-root')).append(entry);
			}
		}
	});
}
