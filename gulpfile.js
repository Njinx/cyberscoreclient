'use strict';

let gulp 			= require('gulp'),
	sass 			= require('gulp-sass'),
	watch 			= require('gulp-watch'),
	sourcemaps		= require('gulp-sourcemaps'),
	esUglify 		= require('uglify-es'),
	postcss			= require('postcss'),
	composer		= require('gulp-uglify/composer'),
	autoprefixer	= require('gulp-autoprefixer'),
	exec			= require('child_process').exec;

let inputSCSS 	= "assets/scss/**/*.scss",
	outputCSS 	= "assets/css/",

	inputJS		= "assets/js/*.js",
	outputJS	= "assets/js/min/",

	maps		= "maps/",

	goSrc		= "**/*.go",

	htmlSrc		= "templates/**/*";

let sentryRelease 	= "1.0";

let esOptions = {
	nameCache	: {},
	compress	: { dead_code: false }
};

var esMinify = composer(esUglify, console);


// Listeners
gulp.task('default', function() {
	gulp.watch(inputSCSS, gulp.series('sassCompile', 'goAssets'));
	gulp.watch(inputJS, gulp.series('esUglify', 'goAssets'));
	gulp.watch(goSrc, gulp.series('goBuild'));
	gulp.watch(htmlSrc, gulp.series('goAssets'));
});

// SASS Compiler
gulp.task('sassCompile', function(cb) {
	return gulp.src(inputSCSS)
		.pipe(sourcemaps.init())
    	.pipe(sass.sync({ outputStyle: 'compressed' })	// Compile SASS to CSS
		.on('error', sass.logError))
		.pipe(autoprefixer())							// Add vendor prefixes to CSS
		.pipe(sourcemaps.write(maps))
    	.pipe(gulp.dest(outputCSS));

		//return exec(`sentry-cli releases -p cyberscoreclient_js files ${sentryRelease} upload-sourcemaps --ext map --rewrite --url-prefix assets/css/maps/ assets/css/maps/`, cb);
});

// JS Minifier
gulp.task('esUglify', function(cb) {
	return gulp.src(inputJS)
		.pipe(sourcemaps.init())
		.pipe(esMinify(esOptions))
		.pipe(sourcemaps.write(maps))
		.pipe(gulp.dest(outputJS));

		//return exec(`sentry-cli releases -p cyberscoreclient_js files ${sentryRelease} upload-sourcemaps --ext map --rewrite --url-prefix assets/js/min/maps/ assets/js/min/maps/`, cb);
});

// Golang Compiler
gulp.task('goBuild', function(cb) {
	return exec("./run.sh", cb); // Run Go build script
});

// Golang Assets Builder
gulp.task('goAssets', function(cb) {
	return exec("./run.sh -a", cb); // Bundles binary assets
})
