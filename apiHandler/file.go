package apiHandler

import (
	"CyberScoreClient/vfs"
	"fmt"
)

// Takes parameter "body" : "pathPath" and responds with the file data
func loadFile(request Request) {
	var resp response
	data, err := vfs.Search(request["body"].(string))

	resp = newResponse(func(r response) response {
		if err != nil {
			fmt.Println(err)
			r.Front.Error = fmt.Sprint(err)
			return r
		} else {
			r.Front.Body = data
			return r
		}
	})

	responseRet <- resp
}
