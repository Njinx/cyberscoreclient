package apiHandler

import (
	"github.com/getsentry/raven-go"
	"encoding/json"
	"log"
)

type Request map[string]interface{}

// Channel for response
var responseRet	= make(chan response)

// Handles the API request
func NewRequest(requestNew []byte, endpoint string) (frontend, backend) {

	// Converts JSON request (byte) into a map
	// TODO: Make the request a proper struct
	var requestMap Request

	requestRaw := json.RawMessage(requestNew)
	err := json.Unmarshal(requestRaw, &requestMap)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Printf("Could not unmarshal request: %v", err)
	}

	go processRequest(requestMap, endpoint)
	// Grabs the response from the final endpoint function
	respRaw	:= <-responseRet

	return respRaw.Front, respRaw.Back
}
