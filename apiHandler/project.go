package apiHandler

import (
	"CyberScoreClient/fileEditor"
	"CyberScoreClient/project"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"

	"github.com/antonholmquist/jason"
)

func submitProjectForm(request Request) {
	const PROJECT_PATH = "config/projects/"

	const CMP_HASH string = "CMP_HASH"
	const CHECK_CMD string = "CHECK_CMD"

	var resp response

	var projectStruct project.Project

	basicDetails, err := json.Marshal(request["basicDetails"])
	if err != nil {
		log.Printf("Unable to serialize project data: %v", err)
	}

	// Bind basicDetails JSON object to Project struct
	json.Unmarshal(basicDetails, &projectStruct.BasicDetails)

	// Serialize project map back to JSON
	// The api.go tries to marshal the original request into a map but we need it in JSON
	// Go can't access nested map values :(
	projectJSON, err := json.Marshal(request)
	if err != nil {
		log.Printf("Unable to serialize project data: %v", err)
	}

	// Encode the project name in hex for the file path
	projectNameHex := hex.EncodeToString([]byte(projectStruct.BasicDetails.ProjectName))

	// Checks if the hex filename is less than 255 characters
	// The maximum allowed filename length is 255
	// The maxmium project name length is 127 because the hex format is 2x longer than the ascii format
	// 1 ASCII character (A) is 2 hex digits (41)
	if len(projectNameHex) > 255 {
		resp = newResponse(func(r response) response {
			errStr := "Project name must be less than 127 characters."
			r.Front.Error = errStr
			log.Printf(errStr)
			return r
		})
		responseRet <- resp
		return
	}

	// Checks if the file already exists
	// Throws an error if true
	if fileEditor.Exists(PROJECT_PATH + projectNameHex + ".json") {
		resp = newResponse(func(r response) response {
			errStr := "A project with that name already exists."
			r.Front.Error = errStr
			log.Printf(errStr)
			return r
		})
		responseRet <- resp
		return
	}

	// Write the project JSON to a new file
	projectFile := fileEditor.NewHandle(PROJECT_PATH + projectNameHex + ".json")
	_, err = projectFile.Write(projectJSON)
	if err != nil {
		log.Printf("Could not write project config to file: %v", err)
	}

	projectFile.Close()

	// Converts the JSON into an object readable by the jason packagee
	projectJason, err := jason.NewObjectFromBytes(projectJSON)
	if err != nil {
		log.Printf("Unable to create object from JSON string: %v", err)
	}

	// Gets the vulnCriterias array
	vulnCriteriasArr, err := projectJason.GetObjectArray("vulnCriterias")
	if err != nil {
		log.Printf("Unable to create array from JSON object: %v", err)
	}

	// Bind each vulnerability to the correct struct and append to vulnCriterias property in Project struct
	for _, vulnObj := range vulnCriteriasArr {
		t, _ := vulnObj.GetString("type")

		// If vulnObj is a file hash comparison
		if t == CMP_HASH {
			var vulnStruct project.CmpHash

			JSONStr, err := vulnObj.Marshal()
			if err != nil {
				log.Printf("Unable to create array from JSON object: %v", err)
			}

			json.Unmarshal(JSONStr, &vulnStruct)

			projectStruct.Vulns = append(projectStruct.Vulns, vulnStruct)

			// If vulnObj is checking command output
		} else if t == CHECK_CMD {
			// TODO

			// Invalid vulnerability type
		} else {
			resp = newResponse(func(r response) response {
				errStr := fmt.Sprintf("Invalid vulnerability type: %s", t)
				r.Front.Error = errStr
				log.Printf(errStr)
				return r
			})
			responseRet <- resp
			return
		}
	}

	resp = newResponse(func(r response) response {
		r.Front.Body = "OK"
		return r
	})
	responseRet <- resp
	return
}

// Returns a JSON array of all project objects
func getProjectsList(request Request) {
	const PROJECTS_PATH = "config/projects/"

	request = nil // No request data is necessary
	var resp response

	nameSlice, err := project.GetProjectsList()
	if err != nil {
		resp = newResponse(func(r response) response {
			errStr := fmt.Sprintf("Could not get the project list: %v", err)
			r.Front.Error = errStr
			log.Println(errStr)
			return r
		})
		responseRet <- resp
		return
	}

	nameJSON, err := json.Marshal(nameSlice)
	if err != nil {
		log.Printf("Could not serialize name slice: %v", err)
	}

	resp = newResponse(func(r response) response {
		r.Front.Body = nameJSON
		return r
	})
	responseRet <- resp
	return
}

func start(request Request) {
	name := request["body"].(string)

	var resp response
	projectState, err := project.StartComp(name)
	if err != nil {
		resp = newResponse(func(r response) response {
			errStr := fmt.Sprintf("Could not start competition: %v", err)
			r.Front.Error = errStr
			log.Println(errStr)
			return r
		})
		responseRet <- resp
		return
	}

	resp = newResponse(func(r response) response {
		r.Front.Body, _ = json.Marshal(projectState)
		return r
	})
	responseRet <- resp
	return
}

func getCompData(request Request) {
	var resp response
	resp = newResponse(func(r response) response {
		r.Front.Body = project.GetCompData()
		return r
	})
	responseRet <- resp
	return
}
