package apiHandler

// Maps endpoint to function
var endpointFuncs = map[string]func(request Request) {
	"simpleResponse"	: simpleResponse,
	"startHTML"			: startHTML,
	"loadFile"			: loadFile,
	"submitProjectForm"	: submitProjectForm,
	"getProjectsList"	: getProjectsList,
	"start"				: start,
	"getCompData"		: getCompData,
}

// Executes the mapped function
func processRequest(request Request, endpoint string) {
	endpointFuncs[endpoint](request)
}
