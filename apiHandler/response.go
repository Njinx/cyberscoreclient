package apiHandler

/*
Usage:
response := newResponse(func(resp response) {
	resp.Front.Body				= "Response for frontend"

	resp.Back.Function.Name		= "ginRedirect"
	resp.Back.Function.Params	= {403,"/403.html"}

	return resp
})

JSON Interpretation:
response : {
	Front : {
		Body : interface{} (Default: nil)
		Error : string (Default: "")
	},
	Back : {
		Function : {
			Name : string (Default: ""),
			Params : []interface{} (Default: nil)
		}
	}
}
*/

// Root response struct
type response struct {
	Front	frontend
	Back	backend
}

// Handled by frontend
type frontend struct {
	Body	interface{}	`json:"body"`	// Response body
	Error	string		`json:"error"`	// Response error
}

// Handled by backend
type backend struct {
	Function	function
}

// Function returned to caller
type function struct {
	Name	string			// Function name
	Params	[]interface{}	// Function parameter
}

// Construct a response by filling in all non-specified values with defaults
func newResponse(initFunc func(resp response) response) response {
	var resp response
	initResp := initFunc(resp) // User defined fields
	var ret response // Initial response

	if initResp.Front.Body != nil {
		ret.Front.Body = initResp.Front.Body
	}
	if initResp.Front.Error != "" {
		ret.Front.Error = initResp.Front.Error
	}
	if initResp.Back.Function.Name != "" {
		ret.Back.Function.Name = initResp.Back.Function.Name
	}
	if initResp.Back.Function.Params != nil {
		ret.Back.Function.Params = initResp.Back.Function.Params
	}

	return ret
}
