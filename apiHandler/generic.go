package apiHandler

// Simple response for API testing
func simpleResponse(request Request) {
	var resp response

	if request["body"] == "ping" {
		resp = newResponse(func(r response) response {
			r.Front.Body = "pong"

			return r
		})
	} else {
		resp = newResponse(func(r response) response {
			r.Front.Body = "Hello World!"

			return r
		})
	}

	responseRet <-resp
}
