package apiHandler

import (
	"encoding/json"
	"log"
	"CyberScoreClient/fileEditor"
)

// Handles new project creation
func startHTML(request Request) {
	configFile := fileEditor.NewHandle("config/local.json")
	byteValue := configFile.Read()

	// Converts the JSON config (byte) into a map
	// TODO: Make this into a proper struct
	var configJson map[string]interface{}

	err := json.Unmarshal(byteValue, &configJson)
	if err != nil {
		log.Printf("Could not deserialize local.json: %v", err)
	}

	var resp response

	if request["body"] == "newProject" {

		// Checks value of newProject in local.json
		if configJson["newProject"] == true {
			configJson["newProject"] = false;

			resp = newResponse(func(r response) response {
				r.Front.Body = "location: new"
				return r
			})
		} else if configJson["newProject"] == false {
			resp = newResponse(func(r response) response {
				r.Front.Body = "location: dashboard"
				return r
			})
		} else {
			resp = newResponse(func(r response) response {
				r.Front.Error = "local.json \"newProject\" isn't bool. Check log."
				log.Fatalf("local.json \"newProject\" is set to %s, should be bool.", configJson["newProject"])
				return r
			})
			responseRet <- resp
			return
		}
	} else if request["body"] == "importProject" {
		// Do stuff here
	} else {
		log.Printf("Invalid request: {\"body\":\"%s\"}", request["body"])
	}

	// Clean up
	configFinal, err := json.Marshal(configJson)
	if err != nil {
		log.Printf("Could not serialize config JSON: %v", err)
	}

	_, err = configFile.Write(configFinal)
	if err != nil {
		log.Printf("Could not write to local.json: %v", err)
	}

	configFile.Close()
	log.Println(resp)
	responseRet <- resp
}
