package autogen

import (
	"CyberScoreClient/vfs"
	"os"
	"log"
)

// Struct describing a filesystem object
//
// isDir: True if the object is a directory, false if is file
// path : String containing the file/folder path
// data : True if the object has data, false if it does not
type object struct {
	isDir	bool
	path	string
	hasData	bool
	data	[]byte
	file	*os.File
}

// List of all files/folders to be generated
var objectList = []object {
	{
		isDir	: true,
		path	: "config",
	},
	{
		isDir	: true,
		path	: "config/projects",
	},
	{
		isDir	: false,
		path	: "config/local.json",
		hasData	: true,
	},
	{
		isDir	: true,
		path	: "tmp",
	},
}

// Checks if the necessary files and folders exists and creates them if needed
func Generate() {

	// Iterates through each object
	for _, v := range objectList {

		// Skip this object if it already exists
		if exists(v) { continue }

		// Check if the object is a folder, if it is, create it then move on as checking for data is unnecessary
		if v.isDir {
			err := os.Mkdir(v.path, os.ModePerm)
			if err != nil {
				log.Printf("Could not create directory: %v", err)
			}
			continue
		}

		file, err := os.Create(v.path)
		if err != nil {
			log.Printf("Unable to create file: %v", err)
			continue
		}
		v.file = file

		// If data exists, retrieve it from vfs then write it to the file
		if v.hasData {
			getData(&v)
			writeData(&v)
		}
	}
}

// Retrieves the object data from vfs
func getData(obj *object) {
	data, err := vfs.Search(obj.path)
	if err != nil {
		log.Printf("Could not retrieve data: %v", err)
	}

	obj.data = data
}

func writeData(obj *object) {
	_, err := obj.file.Write(obj.data)
	if err != nil {
		log.Printf("Could not write data to file: %v", err)
	}
}

// Checks if the object exists and returns a boolean
func exists(obj object) bool {
	if _, err := os.Stat(obj.path); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}
