package fileEditor

import (
	"os"
	"io/ioutil"
	"fmt"
	"log"
	"errors"
)

type Handle struct {
	Path	string		// File path
	Data	[]byte		// Data in file
	Perms	os.FileMode	// File permissions
}

// Initializes the Handle struct
// Accepts a file path and returns a Handle struct
func NewHandle(file string) *Handle {
	h := Handle{
		Path:	file,
		Data:	nil,
	}

	// If file doesn't exist, create it
	if _, err := os.Stat(file); os.IsNotExist(err) {
		os.Create(file)
	}

	h.Perms = getPerms(h.Path)

	return &h
}

// Closes Handle
func (h *Handle) Close() {
	h = nil
}

// Reads h.File and stores the byte data in h.Data then returns it
func (h *Handle) Read() []byte {
	perms := getPerms(h.Path)

	file, err := os.OpenFile(h.Path, os.O_RDONLY, perms)
	if err != nil {
		log.Printf("Could not open file: %v", err)
	}

	h.Data, err = ioutil.ReadAll(file)
	if err != nil {
		log.Printf("Could not read file contents: %v", err)
	}

	file.Close()

	return h.Data
}

// Returns amount in bytes written on succeed and ehandle on fail
func (h *Handle) Write(data []byte) (int, error) {
	perms := getPerms(h.Path)

	// Opens RealFile as writeonly and truncates
	realFile, err := os.OpenFile(h.Path, os.O_WRONLY|os.O_TRUNC, perms)
	if err != nil {
		return 0, err
	}

	wrLen, err := realFile.Write(data)
	rlLen := len(h.Data)

	// Checks if the correct amount of data was written to the file
	if wrLen != rlLen {
		return wrLen, errors.New(fmt.Sprintf("Write corrupted: %d bytes were written out of %d. Stopping.", wrLen, rlLen))
	}

	return wrLen, nil
}

// Takes a file path and checks if it exists
// Returns true if it does, false if it doesn't
func Exists(file string) bool {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}

// Returns the permission bits of h.File
func getPerms(filePath string) os.FileMode {
	stat, err := os.Stat(filePath)
	if err != nil {
		log.Printf("Could not get file permissions: %v", err)
	}

	return stat.Mode()
}
