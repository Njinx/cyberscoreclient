package main

import (
	"CyberScoreClient/apiHandler"
	"CyberScoreClient/project"
	"CyberScoreClient/timeConv"
	"log"

	"github.com/getsentry/raven-go"
	"github.com/gin-gonic/gin"
)

// apiRespFunctionExec executes the closure apiRespFunction
func apiRespFunctionExec(fn func(string, []interface{}), name string, params []interface{}) {
	fn(name, params)
}

var stdAssets = []string{
	"assets/css/main.css",
}

// routeHandlers routes all incoming routes to their respective places
func routeHandlers() {

	// Webroot
	router.GET("/", func(c *gin.Context) {
		initCheck(c)
		c.Done()
	})

	// New installation
	router.GET("/start", func(c *gin.Context) {
		stdAssets = append(stdAssets, "assets/js/min/start.js")
		err := httpPush(c, stdAssets)
		if err != nil {
			log.Printf("Could not push: %v", err)
		}

		c.HTML(200, "start", nil)

		c.Done()
	})

	// New project
	router.GET("/new", func(c *gin.Context) {
		stdAssets = append(stdAssets, "assets/js/min/new.js")
		c.HTML(200, "new", nil)

		err := httpPush(c, stdAssets)
		if err != nil {
			log.Printf("Could not push: %v", err)
		}

		c.Done()
	})

	// CSC Dashboard
	router.GET("/dashboard", func(c *gin.Context) {
		stdAssets = append(stdAssets, "assets/js/min/dashboard.js")
		err := httpPush(c, stdAssets)
		if err != nil {
			log.Printf("Could not push: %v", err)
		}

		c.HTML(200, "dashboard", nil)

		c.Done()
	})

	// Project Dashboard
	router.GET("/project/:name", func(c *gin.Context) {
		var name = c.Param("name")

		stdAssets = append(stdAssets, "assets/js/min/project.js")
		err := httpPush(c, stdAssets)
		if err != nil {
			log.Printf("Could not push: %v", err)
		}

		projectData, err := project.ProjectDataGetter(project.ProjectString(name))
		if err != nil {
			log.Printf("Could not get project data: %v", err)
		}

		c.HTML(200, "project", gin.H{
			"projectHex":  name,
			"projectName": projectData.BasicDetails.ProjectName,
			"maxTime":     timeConv.MillisecondsToHHMMSS(projectData.BasicDetails.MaxTime),
			"refreshInt":  timeConv.MillisecondsToHHMMSS(projectData.BasicDetails.RefreshInt),
		})

		c.Done()
	})

	// Static asset loader
	assets := router.Group("/assets/:aType")
	{
		assets.GET("/:wild/*file", func(c *gin.Context) {
			aType := c.Param("aType") // Asset type (CSS/JS/IMG)
			dir := c.Param("wild")    // Asset directory (bootstrap, fontawesome, fonts, etc.)
			file := c.Param("file")   // Asset file name

			staticPath := StaticPath{
				aType: aType,
				dir:   dir,
				file:  file,
			}

			c.Data(staticLoader(c, staticPath))
			c.Done()
		})

		assets.GET("/:wild", func(c *gin.Context) {
			aType := c.Param("aType") // Asset type (CSS/JS/IMG)
			file := c.Param("wild")   // Asset file name

			staticPath := StaticPath{
				aType: aType,
				file:  file,
			}

			c.Data(staticLoader(c, staticPath))
			c.Done()
		})
	}

	// Router for API calls
	router.POST("/v1/:category/:endpoint", func(c *gin.Context) {
		endpoint := c.Param("endpoint")
		request, err := c.GetRawData()
		if err != nil {
			raven.CaptureErrorAndWait(err, nil)
			log.Printf("Unable to parse request data: %v", err)
		}

		// Sends the endpoint and raw request data (byte) to apiHandler
		frontResp, backResp := apiHandler.NewRequest(request, endpoint)

		// Marshals the response struct and displays it when the parent function terminates
		defer c.JSON(200, &frontResp)

		apiRespFunction := func(name string, params []interface{}) {
			// Matches function name to actual function
			switch name {
			case "": // No function
				break
			case "ginRedirect":
				c.Redirect(params[0].(int), params[1].(string))
			default: // Undefined function
				log.Fatalf("Unknown response function \"%s\"", name)
			}
		}

		// Handles the response backend function (backResp.Function)
		apiRespFunctionExec(apiRespFunction, backResp.Function.Name, backResp.Function.Params)
	})

	// Handles unknown routes
	// TODO: Add more status codes
	router.NoRoute(func(c *gin.Context) {
		requestedURI := c.Request.URL.Host + "/" + c.Request.URL.Path
		c.HTML(404, "404", gin.H{
			"expectedURI": requestedURI,
		})
	})
}
