package project

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"os"
)

// Pairs the vulnerability type to a function, runs the function and returns a vulnStatus struct
func checkVuln(vuln interface{}) {
	var vulnStat vulnStatus

	switch vuln.(type) {
	case CmpHash:
		vulnStat = cmpHash(vuln.(CmpHash))
	case CheckCmd:
		log.Println("checkCmd")
	}

	// If the vulnerability has been fixed
	if vulnStat.PointChange != 0 {
		projectState.Score.PointsCurrent += vulnStat.PointChange

		// If the pointChange is positive, increment the vulnsFixed counter
		// If negative, decrement the counter
		switch {
		case vulnStat.PointChange > 0:
			projectState.Score.VulnsFixed++
		case vulnStat.PointChange < 0:
			projectState.Score.VulnsFixed--
		default:
			log.Println("You should never see this error. I'm a bug, report me.")
		}

		// Mark it as fixed
		projectState.Score.VulnsFixedArray = append(projectState.Score.VulnsFixedArray, vulnStat)
	}
}

// Takes the file path and file hash and checks the files hash against the presupplied hash
// If the hashes are equal, return the a vulnStatus struct with a point change
// If the hashes are not equal, return a vulnStatus struct without a point change
// If an error occurs, return a nil vulnStatus struct
func cmpHash(vuln CmpHash) vulnStatus {
	vulnStat := vulnStatus{
		ID:    vuln.ID,
		Title: vuln.Title,
	}

	// Strip forward slash off of back of file path
	if string(vuln.FilePath[len(vuln.FilePath)-1]) == "/" {
		vuln.FilePath = vuln.FilePath[:len(vuln.FilePath)-1]
	}

	file, err := os.Open(vuln.FilePath)
	defer file.Close()
	if err != nil {
		log.Printf("Could not open file: %v", err)
		return vulnStatus{}
	}

	hash := md5.New()
	_, err = io.Copy(hash, file)
	if err != nil {
		log.Printf("Could not get file hash: %v", err)
		return vulnStatus{}
	}
	fileHash := fmt.Sprintf("%x", hash.Sum(nil))

	// Hashes match, Reward points
	if fileHash == vuln.FileHash {
		vulnStat.PointChange = vuln.PointChange
		return vulnStat
	}

	// Hashes are not equal, Don't reward points
	return vulnStat
}
