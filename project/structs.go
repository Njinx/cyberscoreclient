package project

import "CyberScoreClient/timeConv"

type Name struct {
	Ascii string `json:"ascii"`
	Hex   string `json:"hex"`
}

type Project struct {
	BasicDetails `json:"basicDetails"`
	Vulns        []interface{}
}

type BasicDetails struct {
	ProjectName string        `json:"projectName"`
	MaxPoints   uint16        `json:"maxPoints,string"`
	MaxTime     timeConv.Time `json:"maxTime"`
	RefreshInt  timeConv.Time `json:"refreshInt"`
}

type CmpHash struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	FilePath    string `json:"filePath"`
	FileHash    string `json:"fileHash"`
	PointChange int16  `json:"pointChange"`
}

type CheckCmd struct{}

// Returns an empty Project struct
func ProjectNull() Project {
	return Project{
		BasicDetails: BasicDetails{
			ProjectName: "",
			MaxPoints:   0,
			MaxTime:     0,
			RefreshInt:  0,
		},
		Vulns: nil,
	}
}

type ProjectState struct {
	ID    string     `json:"id"`
	State uint8      `json:"state"`
	Score ScoreBoard `json:"scoreboard"`
}

type ScoreBoard struct {
	TimeRemaining   timeConv.Time `json:"timeRemaining"`
	TimeTotal       timeConv.Time `json:"timeTotal"`
	FinishTime      int64         `json:"finishTime"`
	RefreshInt      timeConv.Time `json:"refreshInt"`
	PointsTotal     int16         `json:"pointsTotal"`
	PointsCurrent   int16         `json:"pointsCurrent"`
	VulnsTotal      int16         `json:"vulnsTotal"`
	VulnsFixed      int16         `json:"vulnsFixed"`
	VulnsFixedArray []vulnStatus  `json:"vulnsFixedArray"`
}

func ProjectStateNull() ProjectState {
	return ProjectState{
		ID:    "",
		State: 0,
		Score: ScoreBoard{
			TimeTotal:       0,
			TimeRemaining:   0,
			FinishTime:      0,
			RefreshInt:      0,
			PointsTotal:     0,
			PointsCurrent:   0,
			VulnsTotal:      0,
			VulnsFixed:      0,
			VulnsFixedArray: nil,
		},
	}
}

// Struct containing the vulnerability status
// ID			: Vulnerability ID
// Title		: Name of the vulnerability
// PointChange	: Amount of points to give/take
type vulnStatus struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	PointChange int16  `json:"pointChange"`
}
