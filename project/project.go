package project

import (
	"CyberScoreClient/fileEditor"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/antonholmquist/jason"
)

// COMP_LOCK Lock if competition is started
const COMP_LOCK string = "tmp/comp.lock"

type ProjectString string
type ProjectJSON []byte

type IProjectDataGetter interface {
	getProjectData() (Project, error)
}

// ProjectDataGetter Returns project data
func ProjectDataGetter(p IProjectDataGetter) (Project, error) {
	projectData, err := p.getProjectData()
	return projectData, err
}

// Takes the project hex name and returns a Project struct
func (name ProjectString) getProjectData() (Project, error) {
	const PROJECT_PATH = "config/projects/"

	const CMP_HASH string = "CMP_HASH"
	const CHECK_CMD string = "CHECK_CMD"

	var projectStruct Project

	// Check if the project exists
	if !fileEditor.Exists(PROJECT_PATH + string(name) + ".json") {
		errStr := fmt.Sprintf("Project %s does not exist.", name)
		err := errors.New(errStr)
		log.Println(err)
		return ProjectNull(), err
	}

	// Reads the project file
	projectFile := fileEditor.NewHandle(PROJECT_PATH + string(name) + ".json")
	projectJSON := projectFile.Read()
	projectFile.Close()

	var projectMap map[string]interface{}
	err := json.Unmarshal(projectJSON, &projectMap)
	if err != nil {
		log.Printf("Unable to marshal project data: %v", err)
	}

	// Bind basicDetails map to Project struct
	basicDetails, _ := json.Marshal(projectMap["basicDetails"])
	json.Unmarshal(basicDetails, &projectStruct.BasicDetails)

	// Converts the JSON into an object readable by the jason packagee
	projectJason, err := jason.NewObjectFromBytes(projectJSON)
	if err != nil {
		log.Printf("Unable to create object from JSON string: %v", err)
	}

	// Gets the vulnCriterias array
	vulnCriteriasArr, err := projectJason.GetObjectArray("vulnCriterias")
	if err != nil {
		log.Printf("Unable to create array from JSON object: %v", err)
	}

	// Bind each vulnerability to the correct struct and append to vulnCriterias property in Project struct
	for _, vulnObj := range vulnCriteriasArr {
		t, _ := vulnObj.GetString("type")

		// If vulnObj is a file hash comparison
		if t == CMP_HASH {
			var vulnStruct CmpHash

			JSONStr, err := vulnObj.Marshal()
			if err != nil {
				log.Printf("Unable to create array from JSON object: %v", err)
			}

			json.Unmarshal(JSONStr, &vulnStruct)

			projectStruct.Vulns = append(projectStruct.Vulns, vulnStruct)

			// If vulnObj is checking command output
		} else if t == CHECK_CMD {
			// TODO

			// Invalid vulnerability type
		} else {
			errStr := fmt.Sprintf("Invalid vulnerability type: %s", t)
			return ProjectNull(), errors.New(errStr)
		}
	}

	return projectStruct, nil
}

// Takes the project JSON and returns a Project struct
func (projectJSON ProjectJSON) getProjectData() (Project, error) {
	const PROJECT_PATH = "config/projects/"

	const CMP_HASH string = "CMP_HASH"
	const CHECK_CMD string = "CHECK_CMD"

	var projectStruct Project

	var projectMap map[string]interface{}
	err := json.Unmarshal(projectJSON, &projectMap)
	if err != nil {
		log.Printf("Unable to marshal project data: %v", err)
	}

	// Bind basicDetails map to Project struct
	basicDetails, _ := json.Marshal(projectMap["basicDetails"])
	json.Unmarshal(basicDetails, &projectStruct.BasicDetails)

	// Converts the JSON into an object readable by the jason packagee
	projectJason, err := jason.NewObjectFromBytes(projectJSON)
	if err != nil {
		log.Printf("Unable to create object from JSON string: %v", err)
	}

	// Gets the vulnCriterias array
	vulnCriteriasArr, err := projectJason.GetObjectArray("vulnCriterias")
	if err != nil {
		log.Printf("Unable to create array from JSON object: %v", err)
	}

	// Bind each vulnerability to the correct struct and append to vulnCriterias property in Project struct
	for _, vulnObj := range vulnCriteriasArr {
		t, _ := vulnObj.GetString("type")

		// If vulnObj is a file hash comparison
		if t == CMP_HASH {
			var vulnStruct CmpHash

			JSONStr, err := vulnObj.Marshal()
			if err != nil {
				log.Printf("Unable to create array from JSON object: %v", err)
			}

			json.Unmarshal(JSONStr, &vulnStruct)

			projectStruct.Vulns = append(projectStruct.Vulns, vulnStruct)

			// If vulnObj is checking command output
		} else if t == CHECK_CMD {
			// TODO

			// Invalid vulnerability type
		} else {
			errStr := fmt.Sprintf("Invalid vulnerability type: %s", t)
			return ProjectNull(), errors.New(errStr)
		}
	}

	return projectStruct, nil
}

// Returns a JSON array of all project objects
func GetProjectsList() ([]Name, error) {
	const PROJECTS_PATH = "config/projects/"

	var nameSlice []Name

	// List files in project directory
	// Returns both ASCII and hex strings
	projects, err := ioutil.ReadDir(PROJECTS_PATH)
	if err != nil {
		errStr := fmt.Sprintf("Error reading project directory: %v", err)
		log.Printf(errStr)
		return nil, err
	}

	for _, project := range projects {
		filename := project.Name()
		nameHex := filename[:len(filename)-5] // Remove .json from filename

		nameASCII := make([]byte, hex.DecodedLen(len([]byte(nameHex))))
		_, err := hex.Decode(nameASCII, []byte(nameHex))
		if err != nil {
			errStr := fmt.Sprintf("Could not decode filename: %v", err)
			log.Printf(errStr)
			return nil, err
		}

		nameStruct := Name{
			Ascii: string(nameASCII),
			Hex:   nameHex,
		}
		nameSlice = append(nameSlice, nameStruct)
	}

	return nameSlice, nil
}

var projectState ProjectState

//  Starts the competition
func StartComp(name string) (ProjectState, error) {

	// Checks if the scoring engine is already running
	if isCompRunning() {
		return ProjectStateNull(), errors.New("There is already a competition in progress.")
	}

	// Get the project data from the project name
	projectData, err := ProjectDataGetter(ProjectString(name))
	if err != nil {
		log.Printf("Could not retrieve project data: %v", err)
		return ProjectStateNull(), err
	}

	// Initialize the ProjectState
	vulnCount := int16(len(projectData.Vulns))

	projectState = ProjectState{
		ID:    name,
		State: 1,
		Score: ScoreBoard{
			TimeTotal:       projectData.BasicDetails.MaxTime,
			TimeRemaining:   projectData.BasicDetails.MaxTime,
			FinishTime:      time.Now().Unix() + (int64(projectData.BasicDetails.MaxTime) / 1000),
			RefreshInt:      projectData.BasicDetails.RefreshInt,
			PointsTotal:     int16(projectData.BasicDetails.MaxPoints),
			PointsCurrent:   0,
			VulnsTotal:      vulnCount,
			VulnsFixed:      0,
			VulnsFixedArray: nil,
		},
	}

	go compClock(projectData)

	return projectState, nil
}

// Returns the current competition data
func GetCompData() ProjectState {
	if projectState.State == 0 {
		return ProjectState{}
	} else {
		return projectState
	}
}

// The competition clock
func compClock(projectData Project) {
	refreshInt := projectState.Score.RefreshInt
	finishTime := projectState.Score.FinishTime

	ticker := time.NewTicker(time.Duration(refreshInt * 1000000))
	defer ticker.Stop()

	// Array of all vulnerabilities
	var vulnStack []interface{}

	// Add all vulnerabilities to vulnStack
	vulnStack = projectData.Vulns

	// Scores every interval
	for tick := range ticker.C {

		// Nullify fields to rescore
		projectState.Score.PointsCurrent,
			projectState.Score.VulnsFixed,
			projectState.Score.VulnsFixedArray = 0, 0, nil

		// Checks each vulnerability
		for _, vuln := range vulnStack {
			checkVuln(vuln)
		}

		projectState.Score.TimeRemaining -= refreshInt

		// Check if time is out
		if tick.Unix() >= finishTime {
			stopScoring(ticker)

			log.Println("Times up!")
		}

	}
}

// Stops the scoring engine
func stopScoring(ticker *time.Ticker) {
	ticker.Stop()
	projectState.State = 0
}

// Checks if a competition is currently in progress
func isCompRunning() bool {
	if projectState.State == 1 {
		return true
	}
	return false
}
