package main

import (
	"github.com/gin-gonic/gin"
	"github.com/getsentry/raven-go"
	"CyberScoreClient/vfs"
	"CyberScoreClient/autogen"
	"log"
)

const port = ":7676"

// New router instance
var router = gin.New()

func main() {

	// Sentry init
	raven.SetDSN("https://2d71d9613eeb4933b713501374878b6b:548cbc269ae34eb8b37f3433aada180c@sentry.io/1430733")
	raven.SetRelease("1.0")

	router.RedirectFixedPath = true;	// Enables redirecting of example.com/var/ to example.com/var and vice versa
	router.Use(gin.Logger())			// Enables logging
	router.Use(gin.Recovery())			// Enables recovering from panics

	t, err := vfs.LoadTemplate() // Loads HTML templates from go-assets
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Printf("Could not load template files: %v", err)
	}

	router.SetHTMLTemplate(t)

	autogen.Generate()	// Generate necessary files/folders

	go routeHandlers()

	err = router.Run(port)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log.Printf("Could not start HTTP routing service: %v", err)
	}
}
