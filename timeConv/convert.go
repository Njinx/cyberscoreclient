package timeConv

import (
	"time"
	"fmt"
	"math"
)

type Time int64

// Takes the time in milliseconds and converts it to a HH:MM:SS formatted string
func MillisecondsToHHMMSS(ms Time) string {
	d, _ := time.ParseDuration(fmt.Sprintf("%vms", ms))

	hrs := math.Floor(d.Hours())					// Converts ms to hours and rounds down
	mins := math.Floor(d.Minutes() - (hrs * 60))	// Converts ms to minutes and subtracts the hours (in minutes)
	secs := math.Floor(d.Seconds() - (mins * 60))	// Converts ms to seconds and subtracts the minutes (in seconds)

	return fmt.Sprintf("%02.0f:%02.0f:%02.0f", hrs, mins, secs)
}
